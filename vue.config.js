const { defineConfig } = require('@vue/cli-service');
module.exports = defineConfig({
    transpileDependencies: true,
    css: {
        loaderOptions: {
            sass: {
                additionalData: `
                  @import "src/assets/scss/_variables.scss";
                  @import "src/assets/scss/_reset.scss";
                  @import "src/assets/scss/_typography.scss";
                `
            }
        }
    },
    publicPath: './',
    chainWebpack: config => {
        config.module
            .rule('images')
            .set('parser', {
                dataUrlCondition: {
                    maxSize: 4 * 1024 // 4KiB
                }
            });
    }
});
